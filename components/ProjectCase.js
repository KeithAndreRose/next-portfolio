import React, { useState } from "react";
import "./styles/ProjectCase.scss";

const ProjectCase = ({ project }) => {
  const [showInfo, setShowInfo] = useState(false);
  const { label, href, description, repoURL } = project;
  console.log(project);

  function toggleInfo() {
    setShowInfo(prevState => !prevState);
  }

  return (
    <>
      <div className="project">
        <h2 className="project-label">
          <a href={href}>{label}</a>
        </h2>
        <div className="project-preview">
          <iframe className="project-preview-content" src={href}></iframe>
        </div>

        <section className={`project-info ${showInfo ? "" : "collapsed"}`}>
          <div className="project-link">
            <b>Site:</b>
            <a href={href}>{href}</a>
          </div>
          <div className="project-repo">
            <b>Repo:</b>
            <a href={repoURL}>{repoURL}</a>
          </div>
          <div className="project-description">
            <b>Description:</b>
            <p>{description}</p>
          </div>
          <span className="project-end">--[END]--</span>
        </section>
        <div className="toggle-info-container">
          <div className="toggle-info" onClick={toggleInfo}>
            {showInfo ? "Show Less" : "Show More"}
          </div>
        </div>
      </div>
    </>
  );
};

export default ProjectCase;
