import React from "react";
import Link from "next/link";
import "./styles/HeaderNavigation.scss";
import "../../assets/styles/global.scss";
import BrandLogo from "../../assets/icons/BrandLogo";
import GithubIcon from "../../assets/icons/GithubIcon";

export default function HeaderNavigation() {
  return (
    <header>
      <nav>
        <div className="brand-wrapper">
          <a href="/">
            <BrandLogo />
          </a>
        </div>
        <div className="nav-links">
          <a
            className="nav-contact-link nav-github-link"
            href="https://github.com/KeithAndreRose"
          >
            <GithubIcon />
          </a>
        </div>
      </nav>
    </header>
  );
}
