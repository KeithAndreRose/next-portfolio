import React from "react";
import Head from "next/head";
import ProjectCase from "../components/ProjectCase";
import SiteFooter from "../components/SiteFooter";
import BrandLogo from "../assets/icons/BrandLogo";
import GithubIcon from "../assets/icons/GithubIcon";
import './styles/index.scss';
import {projects} from '../assets/data/projects'
import HeaderNavigation from "../components/layout/HeaderNavigation";
import ResumeIcon from "../assets/icons/ResumeIcon";
import EmailIcon from "../assets/icons/EmailIcon";

const Home = () => (
  <div>
    <Head>
      <title>Keith Andre Rose | Portfolio</title>
      <link rel="icon" href="/favicon.ico" />
      <meta name="url" content="https://KeithAndreRose.com"/>
      <meta name="description" content="Front-End Engineer | New Jersey"/>
      <meta name="keywords" content="Keith Rose, Keith Andre Rose, Keith, Andre, Rose, Portfolio"/>
      <meta name="image" content="/preview.png"/>
      <meta name="og:title" content="Keith Andre Rose | Portfolio"/>
      <meta name="og:site_name" content="Keith Andre Rose | Portfolio"/>
      <meta name="og:apple-mobile-web-app-title" content="Keith Andre Rose | Portfolio"/>
      <meta name="og:url" content="https://KeithAndreRose.com"/>
      <meta name="og:description" content="Front-End Engineer | New Jersey"/>
      <meta name="og:image" content="/preview.png"/>
      <meta name="twitter:card" content="summary"/>
      <meta name="twitter:title" content="Keith Andre Rose | Portfolio"/>
      <meta name="twitter:url" content="https://KeithAndreRose.com"/>
      <meta name="twitter:description" content="Front-End Engineer | New Jersey"/>
      <meta name="twitter:image" content="/preview.png"/>
      <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png"/>
      <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png"/>
      <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png"/>
      <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png"/>
      <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png"/>
      <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png"/>
      <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png"/>
      <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png"/>
      <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png"/>
      <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png"/>
      <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png"/>
      <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png"/>
      <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png"/>
      <link rel="manifest" href="/manifest.json"/>
      <meta name="msapplication-TileColor" content="#ffffff"/>
      <meta name="msapplication-TileImage" content="/ms-icon-144x144.png"/>
      <meta name="theme-color" content="#ffffff"></meta>
    </Head>

    <HeaderNavigation/>
    <div className="Home-Page">
      <section>
        <div className="hero">
          <h1 className="logo">
            <BrandLogo />
          </h1>
          <h1 className="title">Keith Rose</h1>
          <p className="description">Front-End Engineer | New Jesery</p>

          <div className="links">
            <a
              href="https://github.com/KeithAndreRose"
              target="_blank"
              className="contact-link github"
            >
              <GithubIcon />
              <span className="label">Github</span>
            </a>
            <a
              href="https://onedrive.live.com/embed?cid=F67249D4D232B536&resid=F67249D4D232B536%211218&authkey=AHzdRkzY97Ki3R0&em=2"
              target="_blank"
              className="contact-link resume"
            >
              <ResumeIcon />
              <span className="label">Resume</span>
            </a>
            <a
              href="mailto:keithandrerose@gmail.com?Subject=Hi%Keith!"
              target="_top"
              className="contact-link resume"
            >
              <EmailIcon />
              <span className="label">Contact</span>
            </a>
          </div>
        </div>
      </section>

      <section className="projects">
        <h2 className="projects-feed-label">Projects</h2>
        {projects.map(project => <ProjectCase project={project} key={project.key} />)}
      </section>
      <SiteFooter/>
    </div>
  </div>
);

export default Home;
