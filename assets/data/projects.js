const projects = [
  {
    label: "Gamedev Dictionary",
    href: "https://gamedevdictionary.com",
    description: "Gamedev Dictionary is a free and crowdsourced database of bite-sized video game knowledge.",
    repoURL:'https://github.com/KeithAndreRose/gamedevdictionary.git'
  },
  {
    label: "Simply Task",
    href: "https://simplytask.app",
    description: "Simply Task is simply a task and timer application providing the simplist tools for focused productivity.",
    repoURL:'https://github.com/KeithAndreRose/Simply-Task.git'
  },
  {
    label: "Firebank - Concept & Landing Page",
    href: "https://firebank.app",
    description: "Firebank is a free, non-custodial, ethereum blockchain platform and wallet provider.",
    repoURL:'https://github.com/KeithAndreRose/next-firebank.git'
  }
].map(project => {
  project.key = `project-${project.label}-${project.href}`;
  return project;
});

export {projects}